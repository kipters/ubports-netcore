﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace UbNet
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("App entry point");
            Gtk.Application.Init();
            Console.WriteLine("App init");

            var window = new Gtk.Window("Hello Ubports")
            {
                DefaultSize = new Gdk.Size(400, 300)
            };
            window.Destroyed += (s, e) => Gtk.Application.Quit();

            var ver = Environment.Version.ToString();
            var rtVer = RuntimeInformation.FrameworkDescription;
            var osArch = RuntimeInformation.OSArchitecture;
            var procArch = RuntimeInformation.ProcessArchitecture;
            var osDesc = RuntimeInformation.OSDescription;

            var text = new StringBuilder("Runtime: ")
                .AppendLine(rtVer)
                .Append("Version: ")
                .AppendLine(ver)
                .AppendFormat("{0} process on {1} OS\n\n", procArch, osArch)
                .AppendLine(osDesc)
                .ToString();

            var label = new Gtk.Label(text)
            {
                Halign = Gtk.Align.Center,
                Valign = Gtk.Align.Center,
                LineWrap = true
            };

            var header = new Gtk.HeaderBar
            {
                ShowCloseButton = true,
                Title = "Hello Ubports!"
            };
            window.Titlebar = header;

            window.Add(label);

            Console.WriteLine("UI created");

            window.ShowAll();

            Console.WriteLine("Starting UI loop");

            Gtk.Application.Run();

            Console.WriteLine("KTHXBYE!");
        }
    }
}

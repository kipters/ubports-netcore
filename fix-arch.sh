#!/bin/bash

echo $ARCH

if [ "$ARCH" = "amd64" ];
then
    export DOTNET_RUNTIME_ID=linux-x64
elif [ "$ARCH" = "armhf" ];
then
    export DOTNET_RUNTIME_ID=linux-arm
else
    export DOTNET_RUNTIME_ID=linux-$ARCH
fi


echo $DOTNET_RUNTIME_ID